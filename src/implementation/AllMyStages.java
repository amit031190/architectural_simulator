/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import tools.MyALU;
import utilitytypes.EnumOpcode;
import baseclasses.InstructionBase;
import baseclasses.PipelineRegister;
import baseclasses.PipelineStageBase;
import voidtypes.VoidLatch;
import baseclasses.CpuCore;
import baseclasses.Latch;
import cpusimulator.CpuSimulator;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import utilitytypes.ClockedIntArray;
import static utilitytypes.EnumOpcode.*;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IPipeReg;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.*;
import utilitytypes.IRegFile;
import utilitytypes.Logger;
import utilitytypes.Operand;
import voidtypes.VoidLabelTarget;

/**
 * The AllMyStages class merely collects together all of the pipeline stage
 * classes into one place. You are free to split them out into top-level
 * classes.
 *
 * Each inner class here implements the logic for a pipeline stage.
 *
 * It is recommended that the compute methods be idempotent. This means that if
 * compute is called multiple times in a clock cycle, it should compute the same
 * output for the same input.
 *
 * How might we make updating the program counter idempotent?
 *
 * @author
 */
public class AllMyStages {

    /**
     * * Fetch Stage **
     */
    static class Fetch extends PipelineStageBase {

        public Fetch(ICpuCore core) {
            super(core, "Fetch");
        }

        // Does this state have an instruction it wants to send to the next
        // stage?  Note that this is computed only for display and debugging
        // purposes.
        boolean has_work;

        /**
         * For Fetch, this method only has diagnostic value. However,
         * stageHasWorkToDo is very important for other stages.
         *
         * @return Status of Fetch, indicating that it has fetched an
         * instruction that needs to be sent to Decode.
         */
        @Override
        public boolean stageHasWorkToDo() {
            return has_work;
        }

        @Override
        public String getStatus() {
            IGlobals globals = (GlobalData) getCore().getGlobals();
            if (globals.getPropertyInteger(FETCH_BRANCH_STATE) == GlobalData.BRANCH_STATE_WAITING) {
                addStatusWord("ResourceWait(BranchTargetWait)");
            }
            return super.getStatus();
        }

        @Override
        public void compute(Latch input, Latch output) {
            if (GlobalData.isFetchHalt) {
                return;
            }
            IGlobals globals = (GlobalData) getCore().getGlobals();
            int pc = -1;

            if (globals.getPropertyInteger(FETCH_BRANCH_STATE) == GlobalData.BRANCH_STATE_WAITING) {
                return;
            }
            if (null != globals.getPropertyInteger(CPU_RUN_STATE)) {
                switch (globals.getPropertyInteger(CPU_RUN_STATE)) {
                    case RUN_STATE_HALTING:
                        setActivity("");
                        return;
                    case RUN_STATE_FAULT:
                        addStatusWord("Fault wait");
                        return;
                    case RUN_STATE_FLUSH:
                        return;
                    case RUN_STATE_RECOVERY:
                        addStatusWord("Fault recovery");
                        pc = globals.getPropertyInteger(RECOVERY_PC);
                        globals.setClockedProperty(CPU_RUN_STATE, RUN_STATE_RUNNING);
                        break;
                    case RUN_STATE_RUNNING:
                        // Get the PC and fetch the instruction
                        pc = globals.getPropertyInteger(PROGRAM_COUNTER);
                        break;
                    default:
                        break;
                }
            }

            InstructionBase ins = globals.getInstructionAt(pc);
            has_work = false;
            if (ins.isNull()) {
                // Fetch is working on no instruction at no address
                setActivity("");
            } else {
                // Since there is no input pipeline register, we have to inform
                // the diagnostic helper code explicitly what instruction Fetch
                // is working on.
                has_work = true;
                setActivity(ins.toString());
            }

            EnumOpcode opcode = ins.getOpcode();
            Operand oper0 = ins.getOper0();
            Operand src1 = ins.getSrc1();
            Operand src2 = ins.getSrc2();
            int value0 = 0;
            int value1 = 0;
            switch (opcode) {
                case BRA:
                    if (ins.getLabelTarget().isNull()) {
                        // If branching to address in register, make sure
                        // operand is valid.
                        if (!src1.hasValue()) {
//                                Logger.out.println("Stall BRA wants src1 R" + src1.getRegisterNumber());
                            output.setProperty(LOOKUP_BRANCH_TARGET, 1);
                            // Nothing else to do.  Bail out.
                            globals.setClockedProperty(FETCH_BRANCH_STATE, BRANCH_STATE_WAITING);
                        }

                        value1 = src1.getValue();
                    } else {
                        value1 = ins.getLabelTarget().getAddress();
                    }
                    if (globals.getPropertyBoolean(RECOVERY_TAKEN)) {
                        globals.setClockedProperty(RECOVERY_TAKEN, false);
                        ins.setBranchPrediction(InstructionBase.EnumBranch.TAKEN);
                        globals.setClockedProperty(PROGRAM_COUNTER, value1);
                    } else {
                        if (value1 < pc) {
                            ins.setBranchPrediction(InstructionBase.EnumBranch.TAKEN);
                            globals.setClockedProperty(PROGRAM_COUNTER, value1);

                        } else {
                            ins.setBranchPrediction(InstructionBase.EnumBranch.NOT_TAKEN);
                            globals.setClockedProperty(PROGRAM_COUNTER, pc + 1);

                        }
                    }
                    output.setInstruction(ins);
                    output.copyAllPropertiesFrom(input);
                    output.write();
                    input.consume();

                    return;

                case JMP:
                    // JMP is an inconditionally taken branch.  If the
                    // label is valid, then take its address.  Otherwise
                    // its operand0 contains the target address.
                    if (ins.getLabelTarget().isNull()) {
                        if (!oper0.hasValue()) {
                            // If branching to address in register, make sure
                            // operand is valid.
//                            Logger.out.println("Stall JMP wants oper0 R" + oper0.getRegisterNumber());
                            input.setProperty(LOOKUP_BRANCH_TARGET, 0);
                            globals.setClockedProperty(FETCH_BRANCH_STATE, BRANCH_STATE_WAITING);

                            // Nothing else to do.  Bail out.
                            //return;
                        } else {
                            value0 = oper0.getValue();
                            globals.setClockedProperty(PROGRAM_COUNTER, value0);
                            ins.setBranchPrediction(InstructionBase.EnumBranch.TAKEN);

                        }
                    } else {
                        value0 = ins.getLabelTarget().getAddress();

                        globals.setClockedProperty(PROGRAM_COUNTER, value0);
                        ins.setBranchPrediction(InstructionBase.EnumBranch.TAKEN);
                    }

                    // Since we don't pass an instruction to the next stage,
                    // must explicitly call input.consume in the case that
                    // the next stage is busy.
                    output.setInstruction(ins);
                    output.copyAllPropertiesFrom(input);
                    output.write();
                    input.consume();
                    return;

                case CALL:
                    // CALL is an inconditionally taken branch.  If the
                    // label is valid, then take its address.  Otherwise
                    // its src1 contains the target address.
                    if (!output.canAcceptWork()) {
                        return;
                    }

                    if (ins.getLabelTarget().isNull()) {
                        if (!src1.hasValue()) {
                            // If branching to address in register, make sure
                            // operand is valid.
//                            Logger.out.println("Stall JMP wants oper0 R" + oper0.getRegisterNumber());
                            output.setProperty(LOOKUP_BRANCH_TARGET, 1);

                        } else {
                            value1 = src1.getValue();
                            globals.setClockedProperty(PROGRAM_COUNTER, value1);
                            ins.setBranchPrediction(InstructionBase.EnumBranch.TAKEN);

                        }
                    } else {
                        value1 = ins.getLabelTarget().getAddress();
                        globals.setClockedProperty(PROGRAM_COUNTER, value1);
                        ins.setBranchPrediction(InstructionBase.EnumBranch.TAKEN);
                    }

                    // CALL also has a destination register, which is oper0.
                    // Before we can resolve the branch, we have to make sure
                    // that the return address can be passed to Writeback
                    // through Execute before we go setting any globals.
                    output.setInstruction(ins);
                    output.copyAllPropertiesFrom(input);
                    output.write();
                    input.consume();
                    return;
            }

            if (has_work) {
                output.setInstruction(ins);

            }
            // If the output cannot accept work, then 
            if (!output.canAcceptWork()) {
                return;
            }
            if (opcode == HALT) {
                GlobalData.isFetchHalt = true;
            }
//            Logger.out.println("No stall");
            globals.setClockedProperty(PROGRAM_COUNTER, pc + 1);

        }
    }

    /**
     * * Decode Stage **
     */
    static class Decode extends PipelineStageBase {

        public Decode(ICpuCore core) {
            super(core, "Decode");
        }

        // When a branch is taken, we have to squash the next instruction
        // sent in by Fetch, because it is the fall-through that we don't
        // want to execute.  This flag is set only for status reporting purposes.
        boolean squashing_instruction = false;
        boolean shutting_down = false;

        @Override
        public String getStatus() {
            IGlobals globals = (GlobalData) getCore().getGlobals();
            String s = super.getStatus();
            if (globals.getPropertyBoolean("decode_squash")) {
                s = "Squashing";
            }
            return s;
        }

//        private static final String[] fwd_regs = {"ExecuteToWriteback", 
//            "MemoryToWriteback"};
        @Override
        public void compute() {

            Latch input = readInput(0).duplicate();
            InstructionBase ins = input.getInstruction();
            if (ins.isNull()) {
                return;
            }
            EnumOpcode opcode = ins.getOpcode();
            Operand oper0 = ins.getOper0();

            IGlobals globals = (GlobalData) getCore().getGlobals();
            if (globals.getPropertyInteger(CPU_RUN_STATE) == RUN_STATE_FAULT || globals.getPropertyInteger(CPU_RUN_STATE) == RUN_STATE_HALTING || globals.getPropertyInteger(CPU_RUN_STATE) == RUN_STATE_HALTED) {
                input.consume();
                return;
            }
            int rob_head = globals.getPropertyInteger(ROB_HEAD);
            int rob_tail = globals.getPropertyInteger(ROB_TAIL);
            boolean rob_full = ((rob_tail + 1) & 255) == rob_head;

            if (rob_full) {
                if (oper0.isRegister()) {
                    this.setResourceWait(oper0.getRegisterName() + "ROB is full");
                } else {
                    this.setResourceWait("ROB is full");
                }
                return;
            }
            setActivity(ins.toString());

            IRegFile regfile = globals.getRegisterFile();
            // Start renaming source registers
            ClockedIntArray rat = globals.getPropertyClockedIntArray(REGISTER_ALIAS_TABLE);
            if (ins.getSrc1().isRegister()) {
                int phyRegNum = rat.get(ins.getSrc1().getRegisterNumber());

                ins.getSrc1().rename(phyRegNum);
            }
            if (ins.getSrc2().isRegister()) {
                int phyRegNum = rat.get(ins.getSrc2().getRegisterNumber());
                ins.getSrc2().rename(phyRegNum);
            }
            if (ins.getOpcode().oper0IsSource() && ins.getOper0().isRegister()) {
                int phyRegNum = rat.get(ins.getOper0().getRegisterNumber());
                ins.getOper0().rename(phyRegNum);
            }

            // end renaming source registers
            // This code is to prevent having more than one of the same regster
            // as a destiation register in the pipeline at the same time.
//            if (opcode.needsWriteback()) {
//                int oper0reg = oper0.getRegisterNumber();
//                if (regfile.isInvalid(oper0reg)) {
//                    //Logger.out.println("Stall because dest R" + oper0reg + " is invalid");
//                    setResourceWait("Dest:"+oper0.getRegisterName());
//                    return;
//                }
//            }
//            int phyRegNumFree = -1;
//            for (int i = 0; i < regfile.numRegisters(); i++) {
//                if (!regfile.isUsed(i)) {
//                    phyRegNumFree = i;
//                    break;
//                }
//            }
            // See what operands can be fetched from the register file
            registerFileLookup(input);
            // int phyRegNumFree = rob_tail;
            // See what operands can be fetched by forwarding
            forwardingSearch(input);
            Operand src1 = ins.getSrc1();
            Operand src2 = ins.getSrc2();

            ins.setReorderBufferIndex(rob_tail);
            if (input.hasProperty(LOOKUP_BRANCH_TARGET)) {
                int lookupBranch = (int) input.getPropertyObject(LOOKUP_BRANCH_TARGET);

                if (opcode == EnumOpcode.BRA || opcode == EnumOpcode.CALL) {
//                    if (src1.isRegister() && !src1.hasValue()) {
//                        setResourceWait(src1.getRegisterName());
//                    } else {
//                        globals.setClockedProperty(PROGRAM_COUNTER, src1.getValue());
//                    }
                } else if (opcode == EnumOpcode.JMP) {
                    if (ins.getLabelTarget().isNull()) {
                        if (!oper0.hasValue()) {
                            setResourceWait(oper0.getRegisterName());
                        } else {
                            globals.setClockedProperty(FETCH_BRANCH_STATE, BRANCH_STATE_NULL);
                            globals.setClockedProperty(PROGRAM_COUNTER, oper0.getValue());
                        }
                    }
                }
            }

            if (opcode == EnumOpcode.CALL) {
                Operand pc_operand = Operand.newRegister(Operand.PC_REGNUM);
                pc_operand.setIntValue(ins.getPCAddress());
                ins.setSrc1(pc_operand);
                ins.setSrc2(Operand.newLiteralSource(1));
            }
            Latch output;
            int output_num;
            if (opcode.accessesMemory()) {
                output_num = lookupOutput("DecodeToLSQ");
                output = this.newOutput(output_num);
            } else {
                output_num = lookupOutput("DecodeToIQ");
                output = this.newOutput(output_num);
            }
            // Allocate an output latch for the output pipeline register
            // appropriate for the type of instruction being processed.
            // Latch output;
            // If the desired output is stalled, then just bail out.
            // No inputs have been claimed, so this will result in a
            // automatic pipeline stall.
            if (!output.canAcceptWork()) {
                return;
            }

            // If we managed to find all source operands, mark the destination
            // register invalid then finish putting data into the output latch 
            // and send it.
            // Mark the destination register invalid
            if (opcode.needsWriteback()) {
                int oper0reg = oper0.getRegisterNumber();

                int oldPhyRegNum = rat.get(oper0reg);
                if (oldPhyRegNum < 0) {

                }
                if (oldPhyRegNum > 0) {
                    regfile.markUnmapped(oldPhyRegNum, true);
                }
                regfile.markNewlyAllocated(rob_tail);
                rat.set(oper0reg, rob_tail);
                oper0.rename(rob_tail);
                Logger.out.println("Dest R" + oper0reg + ": " + "R" + oper0reg + " unlinked, " + "P" + rob_tail + " allocated");
            } else {
                regfile.markNewlyAllocated(rob_tail);
            }

            // Copy the forward# properties
            output.copyAllPropertiesFrom(input);
            // Copy the instruction
            output.setInstruction(ins);
            // Send the latch data to the next stage
            output.write();
            InstructionBase[] rob = globals.getPropertyInstructionArr(REORDER_BUFFER);
            rob[rob_tail] = ins;
            if (rob_tail == 255) {
                globals.setProperty(ROB_TAIL, 0);
            } else {
                rob_tail++;
                globals.setProperty(ROB_TAIL, rob_tail);
            }

            // And don't forget to indicate that the input was consumed!
            input.consume();
        }
    }

    /**
     * * Execute Stage **
     */
    static class Execute extends PipelineStageBase {

        public Execute(ICpuCore core) {
            super(core, "Execute");
        }

        @Override
        public void compute(Latch input, Latch output) {
            if (input.isNull()) {
                return;
            }
            doPostedForwarding(input);
            InstructionBase ins = input.getInstruction();

            int source1 = ins.getSrc1().getValue();
            int source2 = ins.getSrc2().getValue();
            int oper0 = ins.getOper0().getValue();

            int result = MyALU.execute(ins.getOpcode(), source1, source2, oper0);

            boolean isfloat = ins.getSrc1().isFloat() || ins.getSrc2().isFloat();
            output.setResultValue(result, isfloat);
            output.setInstruction(ins);
        }
    }

}
