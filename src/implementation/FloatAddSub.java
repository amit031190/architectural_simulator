/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.FunctionalUnitBase;
import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import tools.MultiStageDelayUnit;
import utilitytypes.IFunctionalUnit;
import utilitytypes.IModule;

/**
 *
 * @author amitk
 */
public class FloatAddSub extends FunctionalUnitBase {

    public FloatAddSub(IModule parent, String name) {
        super(parent, name);
    }

    private static class MyFloatAddSubUnit extends PipelineStageBase {

        public MyFloatAddSubUnit(IModule parent) {
            // For simplicity, we just call this stage "in".
            super(parent, "in");
        }

        @Override
        public void compute(Latch input, Latch output) {
            if (input.isNull()) {
                return;
            }
            doPostedForwarding(input);
            InstructionBase ins = input.getInstruction();

            float source1 = Float.intBitsToFloat(ins.getSrc1().getValue());
            float source2 = Float.intBitsToFloat(ins.getSrc2().getValue());
            float result = (float) 0.00;
            switch (ins.getOpcode()) {
                case FADD:
                    result = source1 + source2;
                    break;
                case FSUB:
                    result = source1 - source2;
                    break;
                case FCMP:
                    result = source1 - source2;
                    break;
            }

            output.setResultValue(Float.floatToRawIntBits(result),true);
            output.setInstruction(ins);
        }
    }

    @Override
    public void createPipelineRegisters() {
        createPipeReg("FloatAddSubToDelay");
    }

    @Override
    public void createPipelineStages() {
        addPipeStage(new MyFloatAddSubUnit(this));
    }

    @Override
    public void createChildModules() {
        IFunctionalUnit child = new MultiStageDelayUnit(this, "Delay", 5);
        addChildUnit(child);
    }

    @Override
    public void createConnections() {
        addRegAlias("Delay.out", "out");
        connect("in", "FloatAddSubToDelay", "Delay");
    }

    @Override
    public void specifyForwardingSources() {
        addForwardingSource("out");
    }
}
