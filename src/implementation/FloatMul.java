/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.FunctionalUnitBase;
import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import tools.MultiStageDelayUnit;
import utilitytypes.IFunctionalUnit;
import utilitytypes.IModule;

/**
 *
 * @author amitk
 */
public class FloatMul extends FunctionalUnitBase{
    public FloatMul(IModule parent, String name) {
        super(parent, name);
    }

    private static class MyFloatMulUnit extends PipelineStageBase {
        public MyFloatMulUnit(IModule parent) {
            // For simplicity, we just call this stage "in".
            super(parent, "in");
        }
        
        @Override
        public void compute(Latch input, Latch output) {
            if (input.isNull()) return;
            doPostedForwarding(input);
            InstructionBase ins = input.getInstruction();

            float source1 =Float.intBitsToFloat(ins.getSrc1().getValue());
            float source2 = Float.intBitsToFloat(ins.getSrc2().getValue());

            float result = source1 * source2;
                        
            output.setResultValue(Float.floatToRawIntBits(result),true);
            output.setInstruction(ins);
        }
    }
    
    @Override
    public void createPipelineRegisters() {
        createPipeReg("FloatMulToDelay");  
    }

    @Override
    public void createPipelineStages() {
        addPipeStage(new MyFloatMulUnit(this));
    }

    @Override
    public void createChildModules() {
        IFunctionalUnit child = new MultiStageDelayUnit(this, "Delay", 5);
        addChildUnit(child);
    }

    @Override
    public void createConnections() {
        addRegAlias("Delay.out", "out");
        connect("in", "FloatMulToDelay", "Delay");
    }

    @Override
    public void specifyForwardingSources() {
        addForwardingSource("out");
    }
}
