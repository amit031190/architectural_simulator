/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.CpuCore;
import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import java.util.ArrayList;
import java.util.List;
import utilitytypes.EnumOpcode;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.*;
import utilitytypes.IRegFile;
import utilitytypes.Logger;
import utilitytypes.Operand;

/**
 * * Writeback Stage **
 */
class Writeback extends PipelineStageBase {

    public Writeback(CpuCore core) {
        super(core, "Writeback");
    }
    boolean shutting_down = false;

    @Override
    public void compute() {

        // Things to get from globals:
        // - CPU run state
        // - PRF (type IRegFile)
        // - ROB (type InstructionBase[])
        ICpuCore core = getCore();
        IGlobals globals = (GlobalData) getCore().getGlobals();
        int cpuRunState = globals.getPropertyInteger(CPU_RUN_STATE);
        IRegFile PRF = globals.getRegisterFile();
        InstructionBase[] ROB = globals.getPropertyInstructionArr(REORDER_BUFFER);
        // Loop over all input ports, and for each input....
        // Skip over any inputs without valid instruction
        // Writeback has multiple inputs, so we just loop over them
        int num_inputs = this.getInputRegisters().size();
        List<String> doing = new ArrayList<String>();

        for (int i = 0; i < num_inputs; i++) {
            // Get the input by index and the instruction it contains
            Latch input = readInput(i);

            // Skip to the next iteration of there is no instruction.
            if (input.isNull()) {
                continue;
            }

            InstructionBase ins = input.getInstruction();

            // Update ROB entry to latest state of instruction.
            int ROB_PRF_index = ins.getReorderBufferIndex();
            core.incCompleted();
            doing.add(ins.toString());
            // (The entry is originally filled with the freshly renamed instruction.
            // Once it reaches Writeback, it now has all of its inputs and any
            // computed result value.)
            // call core.incCompleted()
            // If the run state is RUN_STATE_FLUSH *and* core.numCompleted()==core.numIssued())
            // then change CPU run state to RUN_STATE_RECOVERY.
            // If a completing instruction needs writeback, copy the result value from
            // the instruction to the PRF entry.  For diagnostic purposes, also 
            // mark the PRF entry as being a FLOAT if the result is a float.

            if (cpuRunState == RUN_STATE_FLUSH && core.numCompleted() == core.numIssued()) {
                globals.setClockedProperty(CPU_RUN_STATE, RUN_STATE_RECOVERY);
            } else {

                if (cpuRunState != RUN_STATE_RECOVERY) {
                    ROB[ROB_PRF_index] = ins;

                }
            }
            Operand op = ins.getOper0();
            String regname = op.getRegisterName();
            int regnum = op.getRegisterNumber();
            int value = input.getResultValue();
            boolean isfloat = input.isResultFloat();

            // If the completing instruction DOES NOT need writeback, simply mark
            // the PRF entry as valid.
            // If the CPU run state is RUN_STATE_RUNNING and the completing instruction
            // has a fault, then mark its PRF entry as having a fault.
            // (Or you can just rely on the fact that updating the ROB entry
            // also carries with it fault information.)
            // Change the run state to RUN_STATE_FAULT.
            // Don't forget to consume the input.
            // Set the activity string to indicate all instructions just completed
            if (ins.getOpcode().needsWriteback()) {

                addStatusWord(regname + "=" + input.getResultValueAsString());
                PRF.setValue(regnum, value, isfloat);
                PRF.markValid(ROB_PRF_index);
            } else {
                PRF.markValid(ROB_PRF_index);
            }

            if (cpuRunState == RUN_STATE_RUNNING && ins.getFault() == InstructionBase.EnumFault.BRANCH) {
                PRF.markFault(ROB_PRF_index, true);
                globals.setClockedProperty(CPU_RUN_STATE, RUN_STATE_FAULT);
            }
            input.consume();
        }
        setActivity(String.join("\n", doing));

    }

}
