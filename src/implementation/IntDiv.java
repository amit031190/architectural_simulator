/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.EnumOpcode;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.CPU_RUN_STATE;

/**
 *
 * @author amitk
 */
public class IntDiv extends PipelineStageBase {

    public IntDiv(IModule parent) {
        super(parent, "IntDiv");
    }
    private int stallCount = 0;

    @Override
    public void compute(Latch input, Latch output) {
        if (input.isNull()) {
            return;
        }
        IGlobals globals = (GlobalData) getCore().getGlobals();
        InstructionBase ins = input.getInstruction();
        if (globals.getPropertyInteger(CPU_RUN_STATE) == IProperties.RUN_STATE_FLUSH) {
            stallCount = 15;
        }
        doPostedForwarding(input);
        if (stallCount < 15) {
            stallCount++;
            this.setResourceWait("StallCount:" + stallCount);
            return;
        }
        stallCount = 0;

        int source1 = ins.getSrc1().getValue();
        int source2 = ins.getSrc2().getValue();
        int result = 0;
        if (ins.getOpcode() == EnumOpcode.DIV) {
            result = source1 / source2;
        } else if (ins.getOpcode() == EnumOpcode.MOD) {
            result = source1 % source2;
        }

        output.setResultValue(result);
        output.setInstruction(ins);
    }
}
