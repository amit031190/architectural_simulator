/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import java.util.ArrayList;
import java.util.List;
import utilitytypes.IProperties;

/**
 *
 * @author amitk
 */
public class IssueQueueTbl {

    private final List<InstructionBase> values;
    //private final boolean[] inValidFlags;
    private final int tblSize;

    public IssueQueueTbl(int size) {
        values = new ArrayList<InstructionBase>();
        //inValidFlags = new boolean[size];
        tblSize = size;
    }

    public int getSize() {
        return values.size();
    }

    public boolean isFreeSlotAvailable() {
        return values.size() < tblSize;
    }
//    public boolean isValid(int num) {
//        return !inValidFlags[num];
//    }

//    public void setInValid(int num, boolean flag) {
//        inValidFlags[num] = flag;
//    }
    public InstructionBase getInstruction(int num) {
        return values.get(num);
    }

    public void addInstruction(InstructionBase ins) {
        values.add(ins);
    }

    public void freeSlot(int num) {
        values.remove(num);
    }
    
     public void flushIQ() {
        values.clear();
    }
}
