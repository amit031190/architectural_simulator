/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import utilitytypes.ClockedIntArray;
import utilitytypes.EnumOpcode;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IPipeStage;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.*;
import utilitytypes.IRegFile;
import utilitytypes.Logger;
import utilitytypes.Operand;
import utilitytypes.RegisterFile;

/**
 *
 * @author millerti
 */
public class Retirement extends PipelineStageBase {

    public Retirement(IModule parent) {
        super(parent, "Retirement");
        disableTwoInputCompute();
    }

    @Override
    public void compute() {

        List<String> doing = new ArrayList<String>();
        ICpuCore core = getCore();
        IGlobals globals = core.getGlobals();

        /*
        From Globals, you will need to get the following:
        - Reorder buffer (of type InstructionBase[])
        - PRF (of type IRegFile)
        - ARF (of type IRegFile)
        - RAT (of type ClockedIntArray)
        - CPU run state
        - ROB head pointer
        - Possibly also ROB tail pointer and/or used entry count
         */
        InstructionBase[] ROB = globals.getPropertyInstructionArr(REORDER_BUFFER);
        IRegFile PRF = globals.getRegisterFile();
        IRegFile ARF = globals.getPropertyRegisterFile(ARCH_REG_FILE);
        ClockedIntArray RAT = globals.getPropertyClockedIntArray(REGISTER_ALIAS_TABLE);
        int cpuRunState = globals.getPropertyInteger(CPU_RUN_STATE);
        int rob_head = globals.getPropertyInteger(ROB_HEAD);
        int rob_tail = globals.getPropertyInteger(ROB_TAIL);

        // In RUN_STATE_FLUSH, reset ROB head to same as tail and 
        // anything else necessary to mark the ROB as empty.
        if (cpuRunState == RUN_STATE_FLUSH) {
            rob_head = rob_tail;
            globals.setProperty(ROB_HEAD, rob_head);
            // Also, clear all RAT entries to -1 (indicating that arch regs
            // map to the ARF).
            for (int i = 0; i < 32; i++) {
                RAT.set(i, -1);
            }

            for (int i = 0; i < ROB.length; i++) {
                ROB[i] = null;
            }
        }
        Set<Integer> retiredSet = new HashSet<Integer>();
        for (int i = rob_head; i < ROB.length; i++) {
            InstructionBase ins = ROB[i];
            if (ins == null) {
                continue;
            }
            int regnum = ins.getOper0().getRegisterNumber();
            Operand oper0 = ins.getOper0();
            EnumOpcode opcode = ins.getOpcode();
            if (PRF.isValid(i)) {

                doing.add(ins.toString());
                switch (opcode) {
                    case OUT:
                        // It doesn't really matter which stage OUT is processed in.
                        // I did it in Execute.  Some people did it in Writeback.
                        // Since OUT is inspired by an I/O instruction in real 
                        // CPUs, possibly the most "purist" stage to execute it in
                        // would be Memory.
                        Logger.out.println("@@output: " + oper0.getValue());
                        break;

                    case FOUT:
                        Logger.out.println("@@output: " + Float.intBitsToFloat(oper0.getValue()));
                        break;

                }
                if (opcode == EnumOpcode.HALT) {
                    globals.setClockedProperty(CPU_RUN_STATE, RUN_STATE_HALTING);
                }
                if (ins.getFault() == InstructionBase.EnumFault.BRANCH) {
                    globals.setClockedProperty(CPU_RUN_STATE, RUN_STATE_FLUSH);
                    globals.setClockedProperty(RECOVERY_PC, ins.getPCAddress());
                    if (ins.getBranchResolution() == InstructionBase.EnumBranch.TAKEN) {
                        globals.setClockedProperty(RECOVERY_TAKEN, true);
                    } else {
                        globals.setClockedProperty(RECOVERY_TAKEN, false);
                    }
                    break;
                }
                retiredSet.add(i);
                PRF.markRetired(i, true);
                if (opcode.needsWriteback()) {
                    int orgArchRegNum = ins.getOper0().getOrigRegisterNumber();
                    ARF.setValue(orgArchRegNum, PRF.getValue(i), PRF.isFloat(i));
                    if (RAT.get(orgArchRegNum) == i) {
                        RAT.set(orgArchRegNum, -1);
                    };
                }
                rob_head++;
                globals.setProperty(ROB_HEAD, rob_head);
            } else {
                break;
            }
        }
        core.putRetiredSet(retiredSet);
        setActivity(String.join("\n", doing));
        // Starting from the ROB head, loop until you either have processed
        // all entries or encounter an entry whose PRF register is not marked
        // valid.  Your code should retire ALL completed entries at the head
        // of the ROB....
        // In this loop:
        // This is where you do the actual printing for OUT and FOUT.
        // The HALT instruction puts the processor into the RUN_STATE_HALTING
        //   state, where the only thing that is allowed to happen is for any
        //   remaining uncommitted stores to retire.  The LSQ (or rather the
        //   DCache) is what will change the run state to RUN_STATE_HALTED
        //   once all STOREs are retired.
        // If the instruction at the head of the queue has a fault:
        // - Set the run state to RUN_STATE_FLUSH
        // - Set the global property RECOVERY_PC to that of the faulting instruction
        // - Set the global property RECOVERY_TAKEN to the CORRECT resolution of the branch.
        // Add retired instructions to a set so that the LSQ can tell which
        // STOREs need to be retired.
        // For diagnostic purposes, mark the PRF entry corresonding to the
        // ROB head as retired.
        // If the instruction being retired needs a writeback, copy the
        // result value from the PRF to the ARF.
        // If the RAT entry for this arch register points to the head of the
        // ROB, set the RAT entry to -1 to indicate that the only instance
        // of this arch reg has been retired.
        // If the instruction being retired does NOT need a writeback,
        // take no special action on the RAT or ARF.
        // Increment the head pointer, wrapping around the end of the ROB.
        // Optionally, update the used-entry count.
        // Quit the loop of the new head instruction is not completed.
        // Use core.putRetiredSet() to provide the set of instructions retired
        // this cycle.
        // Set the activity string with the contents of ROB.

    }
}
