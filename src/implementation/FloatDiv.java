/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.CPU_RUN_STATE;

/**
 *
 * @author amitk
 */
public class FloatDiv extends PipelineStageBase {

    public FloatDiv(IModule parent) {
        super(parent, "FloatDiv");
    }
    private int stallCount = 0;

    @Override
    public void compute(Latch input, Latch output) {
        if (input.isNull()) {
            return;
        }
        IGlobals globals = (GlobalData) getCore().getGlobals();
        InstructionBase ins = input.getInstruction();
        if (globals.getPropertyInteger(CPU_RUN_STATE) == IProperties.RUN_STATE_FLUSH) {
            stallCount = 15;
        }
        doPostedForwarding(input);
        if (stallCount < 15) {
            stallCount++;
            this.setResourceWait("StallCount:" + stallCount);
            return;
        }
        stallCount = 0;

        float source1 = Float.intBitsToFloat(ins.getSrc1().getValue());
        float source2 = Float.intBitsToFloat(ins.getSrc2().getValue());
        float result = source1 / source2;
        output.setResultValue(Float.floatToRawIntBits(result), true);
        output.setInstruction(ins);
    }
}
