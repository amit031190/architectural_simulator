/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.InstructionBase.EnumBranch;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.EnumComparison;
import utilitytypes.IModule;
import utilitytypes.IProperties;

/**
 *
 * @author millerti
 */
public class BranchResUnit extends PipelineStageBase {

    public BranchResUnit(IModule parent) {
        super(parent, "BranchResUnit");
    }

    static boolean resolveBranch(EnumComparison condition, int value0) {
        // Add code here...
        boolean take_branch = false;
        switch (condition) {
            case EQ:
                take_branch = (value0 == 0);
                break;
            case NE:
                take_branch = (value0 != 0);
                break;
            case GT:
                take_branch = (value0 > 0);
                break;
            case GE:
                take_branch = (value0 >= 0);
                break;
            case LT:
                take_branch = (value0 < 0);
                break;
            case LE:
                take_branch = (value0 <= 0);
                break;
        }
        return take_branch;
    }

    @Override
    public void compute(Latch input, Latch output) {
        if (input.isNull()) {
            return;
        }
        doPostedForwarding(input);
        InstructionBase ins = input.getInstruction().duplicate();

        /*
        JMP -- pass through
        BRA -- resolve, compare to prediction, set fault on instruction for disagreement
        CALL -- compute return address and pass on as result value.
         */
        switch (ins.getOpcode()) {
            case JMP:
                break;
            case CALL:
                int source1 = ins.getSrc1().getValue();
                int source2 = ins.getSrc2().getValue();
                int result = source1 + source2;
                boolean isfloat = ins.getSrc1().isFloat() || ins.getSrc2().isFloat();
                output.setResultValue(result, isfloat);
                break;
            case BRA:
                if (resolveBranch(ins.getComparison(), ins.getOper0().getValue())) {
                    ins.setBranchResolution(EnumBranch.TAKEN);
                } else {
                    ins.setBranchResolution(EnumBranch.NOT_TAKEN);
                }

                if (ins.getBranchPrediction() != ins.getBranchResolution()) {
                    ins.setFault(InstructionBase.EnumFault.BRANCH);
                }
                break;
        }
        output.setInstruction(ins);
    }

}
