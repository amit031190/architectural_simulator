/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.CpuCore;
import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import static baseclasses.PipelineStageBase.operNames;
import cpusimulator.CpuSimulator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import utilitytypes.EnumOpcode;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IPipeReg;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.*;
import utilitytypes.Logger;
import utilitytypes.Operand;

/**
 *
 * @author millerti
 */
public class LoadStoreQueue extends PipelineStageBase {

    public LoadStoreQueue(IModule parent) {
        super(parent, "LoadStoreQueue");

        // Force PipelineStageBase to use the zero-operand compute() method.
        this.disableTwoInputCompute();
    }

    // Data structures
    public void compute() {
        // First, mark any retired STORE instructions
        ICpuCore core = getCore();
        Set<Integer> retiredSet = core.getRetiredSet();
        IGlobals globals = (GlobalData) getCore().getGlobals();
        InstructionBase[] lsq = globals.getPropertyInstructionArr(GlobalData.lsq);
        for (int i = 0; i < lsq.length; i++) {
            if (lsq[i] != null) {
                if (retiredSet != null && retiredSet.contains(lsq[i].getReorderBufferIndex())) {
                    lsq[i].setProperty("RETIRED", 1);
                }
            }
        }
        Latch input = this.readInput(0).duplicate();
        Latch output = newOutput(0);
        InstructionBase ins = input.getInstruction();
        // Check CPU run state
        if (globals.getPropertyInteger(CPU_RUN_STATE) == RUN_STATE_FAULT) {
            input.consume();
            input.setInvalid();
        } else if (globals.getPropertyInteger(CPU_RUN_STATE) == RUN_STATE_FLUSH || globals.getPropertyInteger(CPU_RUN_STATE) == RUN_STATE_HALTING) {
            input.consume();
            input.setInvalid();
            for (int i = 0; i < lsq.length; i++) {
                if (lsq[i] != null) {
                    if (!lsq[i].hasProperty("RETIRED")) {
                    } else {
                        lsq[i] = null;
                        for (int shiftIndex = i; shiftIndex < lsq.length - 1; shiftIndex++) {
                            lsq[shiftIndex] = lsq[shiftIndex + 1];
                        }
                        lsq[lsq.length - 1] = null;
                        i--;
                    }
                }
            }
        }
        if (globals.getPropertyInteger(CPU_RUN_STATE) == RUN_STATE_HALTING) {
            boolean isRetStoreleft = false;
            for (int i = 0; i < lsq.length; i++) {
                if (lsq[i] != null) {
                    if (lsq[i].hasProperty("RETIRED")) {
                        isRetStoreleft = true;
                        break;
                    }
                }
            }
            if (!isRetStoreleft) {
                globals.setClockedProperty(CPU_RUN_STATE, RUN_STATE_HALTED);
                return;
            }
        }
        // If the LSQ is full and a memory instruction wants to come in from
        // Decode, optionally see if you can proactively free up a retired STORE.
        // See if there's room to storea an instruction received from Decode.
        // Read the input latch.  If it's valid, put the instruction into the 
        // LAST slot.
        // LSQ entries are stored in order from oldest to newest.          
        if (!input.isNull()) {
            boolean isFreeEntry = false;
            int freeIndex = 0;
            for (int i = 0; i < lsq.length; i++) {
                if (lsq[i] == null) {
                    isFreeEntry = true;
                    freeIndex = i;
                    break;
                }
            }
            if (isFreeEntry) {
                lsq[freeIndex] = ins;
                input.consume();
            }
        }

        // Loop over the forwarding sources, capturing all values available
        // right now and storing them into LSQ entries needing those
        // register values.
        Set<String> fwdSources = core.getForwardingSources();
        for (int i = 0; i < lsq.length; i++) {

            InstructionBase inslsq = lsq[i];
            if (inslsq == null) {
                break;
            }
            EnumOpcode opcode = inslsq.getOpcode();
            boolean oper0src = opcode.oper0IsSource();

            Operand oper0 = inslsq.getOper0();
            Operand src1 = inslsq.getSrc1();
            Operand src2 = inslsq.getSrc2();
            // Put operands into array because we will loop over them,
            // searching the pipeline for forwarding opportunities.
            Operand[] operArray = {oper0, src1, src2};

            // For operands that are not registers, getRegisterNumber() will
            // return -1.  We will use that to determine whether or not to
            // look for a given register in the pipeline.
            int[] srcRegs = new int[3];
            // Only want to forward to oper0 if it's a source.
            srcRegs[0] = oper0src ? oper0.getRegisterNumber() : -1;
            srcRegs[1] = src1.getRegisterNumber();
            srcRegs[2] = src2.getRegisterNumber();

            for (int sn = 0; sn < 3; sn++) {
                int srcRegNum = srcRegs[sn];
                // Skip any operands that are not register sources
                if (srcRegNum < 0) {
                    continue;
                }
                // Skip any operands that already have values
                if (operArray[sn].hasValue()) {
                    continue;
                }
                Operand oper = operArray[sn];
                String srcRegName = oper.getRegisterName();
                String operName = operNames[sn];

                String srcFoundIn = null;
                boolean next_cycle = false;

                prn_loop:
                for (String fwd_pipe_reg_name : fwdSources) {
                    IPipeReg.EnumForwardingStatus fwd_stat = core.matchForwardingRegister(fwd_pipe_reg_name, srcRegNum);

                    switch (fwd_stat) {
                        case NULL:
                            break;
                        case VALID_NOW:
                            srcFoundIn = fwd_pipe_reg_name;
                            break prn_loop;
                        case VALID_NEXT_CYCLE:
                            srcFoundIn = fwd_pipe_reg_name;
                            next_cycle = true;
                            break prn_loop;
                    }
                }

                if (srcFoundIn != null) {
                    if (!next_cycle) {
                        // If the register number was found and there is a valid
                        // result, go ahead and get the value.
                        int value = core.getResultValue(srcFoundIn);
                        boolean isfloat = core.isResultFloat(srcFoundIn);
                        operArray[sn].setValue(value, isfloat);

                        if (CpuSimulator.printForwarding) {
                            Logger.out.printf("# Forwarding %s=%s from %s to %s of %s\n",
                                    srcRegName, oper.getValueAsString(),
                                    srcFoundIn, operName,
                                    getHierarchicalName());
                        }
                    } else {
                        // Post forwarding for the next stage on the next cycle by
                        // setting a property on the latch that specifies which
                        // operand(s) is forwarded from what pipeline register.
                        // For instance, setting the property "forward1" to the 
                        // value "ExecuteToWriteback" will inform the next stage
                        // to get a value for src1 from ExecuteToWriteback.
                        String propname = "forward" + sn;
                        inslsq.setProperty(propname, srcFoundIn);

//                    if (CpuSimulator.printForwarding) {
//                        Logger.out.printf("Posting forward %s from %s to %s next stage\n", 
//                                srcRegName,
//                                srcFoundIn, operName);
//                    }
                    }
                }
            }
        }
        // For diagnostic purposes, iterate existing entries of the LSQ and add 
        // their current state to the activity list
        HashMap<Integer, String> diagLsq = new HashMap<Integer, String>();
        boolean isLoadready = false;
        for (int i = 0; i < lsq.length; i++) {
            if (lsq[i] != null) {

                switch (lsq[i].getOpcode()) {
                    case LOAD:
                        if (lsq[i].getSrc1().hasValue()) {
                            diagLsq.put(lsq[i].getReorderBufferIndex(), lsq[i].toString() + " [ready]");
                            isLoadready = true;
                        } else {
                            diagLsq.put(lsq[i].getReorderBufferIndex(), lsq[i].toString());
                        }
                        break;
                    case STORE:
                        if (lsq[i].hasProperty("RETIRED")) {
                            diagLsq.put(lsq[i].getReorderBufferIndex(), lsq[i].toString() + " [retired]");
                        } else if (lsq[i].hasProperty("COMPLETED")) {
                            diagLsq.put(lsq[i].getReorderBufferIndex(), lsq[i].toString() + " [completed]");
                        } else if (lsq[i].getSrc1().hasValue()) {
                            diagLsq.put(lsq[i].getReorderBufferIndex(), lsq[i].toString() + " [ready]");
                        } else {
                            diagLsq.put(lsq[i].getReorderBufferIndex(), lsq[i].toString());
                        }
                        break;
                }
            }
        }
//      
        // Next, try to do things that require writing to the
        // next pipeline stage.  If we have already written the output, or
        // the output can't accept work, might as well bail out.

// If and only if there is a LOAD in the FIRST entry of the LSQ, it can be
        // issued to the DCache if needed inputs (to compute the address)
        // will be available net cycle.  
        // Set appropriate "forward#" properties on output latch.
        // Only when a LOAD is not prededed by STOREs can be be issued with
        // forwarding in the next cycle.
        // ******
        // When issuing any LOAD, other entries in the LSQ must be shifted
        // to fill the gap.  The LSQ must maintain program order.  This applies
        // to all cases where the LSQ issues a LOAD.
        // ******
        // If we issued a load, bail out.  ** Before bailing out, ALWAYS make sure
        // that setActivity has been called with info about all the 
        // instructons in the queue.  This is for diagnostic purposes. **
        // Look for a load whose address matches that of a store earlier in the list.
        // Since we don't do speculative loads, if a store is encountered with an
        // unknown address, then no subsequent loads can be issued.
        // Outer loop:  Iterate over all LOAD instructions from first to last
        // LSQ entries.
        // Inner loop:  Iterate backwards over STOREs that came before the LOAD.
        // If you find a STORE with a unknown address, skip to the next LOAD
        // in the outer loop.
        // If you find a STORE with a matching address, make sure the STORE
        // has a data value. If it does, this LOAD can be ussued as a BYPASS LOAD:
        // copy the value from the STORE to the LOAD and issue the load, 
        // instructing the DCache to NOT fetch from memory.
        // If the STORE does not have a data value, skip to the next LOAD in 
        // the outer loop.  
        // Data that is forwarded from a STORE to a LOAD must some from the
        // matching STORE that is NEAREST to the LOAD in the list.
        // If the inner loop finishes and finds neither a matching STORE addresss
        // nor an unknown store address, this LOAD can be issued as an ACCESS
        // LOAD:  The data is fetched from main memory.
        // If we issued a LOAD, set activity string, bail out
        boolean wrote_output = false;
        if (lsq[0] != null && lsq[0].getOpcode() == EnumOpcode.LOAD) {

            InstructionBase inslsq = lsq[0];
            if (inslsq != null) {

                EnumOpcode opcode = inslsq.getOpcode();
                boolean oper0src = opcode.oper0IsSource();

                Operand oper0 = inslsq.getOper0();
                Operand src1 = inslsq.getSrc1();
                Operand src2 = inslsq.getSrc2();
                // Put operands into array because we will loop over them,
                // searching the pipeline for forwarding opportunities.
                Operand[] operArray = {oper0, src1, src2};

                // For operands that are not registers, getRegisterNumber() will
                // return -1.  We will use that to determine whether or not to
                // look for a given register in the pipeline.
                int[] srcRegs = new int[3];
                // Only want to forward to oper0 if it's a source.
                srcRegs[0] = oper0src ? oper0.getRegisterNumber() : -1;
                srcRegs[1] = src1.getRegisterNumber();
                srcRegs[2] = src2.getRegisterNumber();

                boolean islsQValid = true;
                for (int sn = 0; sn < 3; sn++) {
                    int srcRegNum = srcRegs[sn];
                    // Skip any operands that are not register sources
                    if (srcRegNum < 0) {
                        continue;
                    }
                    // Skip any that already have values
                    if (operArray[sn].hasValue()) {
                        continue;
                    }

                    String propname = "forward" + sn;
                    if (!inslsq.hasProperty(propname)) {
                        // If any source operand is not available
                        // now or on the next cycle, then stall.
                        //Logger.out.println("Stall because no " + propname);
                        //this.setResourceWait(operArray[sn].getRegisterName());
                        // Nothing else to do.  Bail out.
                        islsQValid = false;
                        break;
                    } else {

                    }
                }
                if (islsQValid) {
                    output.setProperty("ACCESS LOAD", 1);
                    output.setInstruction(inslsq);

                    // Send the latch data to the next stage
                    output.write();
                    output.copyAllPropertiesFrom(inslsq);
                    core.incIssued();
                    wrote_output = true;
                    lsq[0] = null;
                    for (int shiftIndex = 0; shiftIndex < lsq.length - 1; shiftIndex++) {
                        lsq[shiftIndex] = lsq[shiftIndex + 1];
                    }
                    lsq[lsq.length - 1] = null;
                }
            }
        }
        if (wrote_output || !outputCanAcceptWork(0)) {
            setActivityLocal(diagLsq);
            return;
        }

        int loadIndex = -1;
        Operand store_oper0 = null;
        boolean matchStoreNotFound = false;
        for (int i = 0; i < lsq.length; i++) {
            InstructionBase inslsq = lsq[i];
            if (inslsq != null) {
                EnumOpcode opcode = inslsq.getOpcode();
                boolean oper0src = opcode.oper0IsSource();

                Operand oper0 = store_oper0 = inslsq.getOper0();
                Operand src1 = inslsq.getSrc1();
                Operand src2 = inslsq.getSrc2();
                // Put operands into array because we will loop over them,
                // searching the pipeline for forwarding opportunities.
                Operand[] operArray = {oper0, src1, src2};

                // For operands that are not registers, getRegisterNumber() will
                // return -1.  We will use that to determine whether or not to
                // look for a given register in the pipeline.
                int[] srcRegs = new int[3];
                // Only want to forward to oper0 if it's a source.
                srcRegs[0] = oper0src ? oper0.getRegisterNumber() : -1;
                srcRegs[1] = src1.getRegisterNumber();
                srcRegs[2] = src2.getRegisterNumber();

                boolean islsQValid = true;
                for (int sn = 0; sn < 3; sn++) {
                    int srcRegNum = srcRegs[sn];
                    // Skip any operands that are not register sources
                    if (srcRegNum < 0) {
                        continue;
                    }
                    // Skip any that already have values
                    if (operArray[sn].hasValue()) {
                        continue;
                    }
                    String propname = "forward" + sn;
                    if (!inslsq.hasProperty(propname)) {
                        // If any source operand is not available
                        // now or on the next cycle, then stall.
                        //Logger.out.println("Stall because no " + propname);
                        //this.setResourceWait(operArray[sn].getRegisterName());
                        // Nothing else to do.  Bail out.
                        islsQValid = false;
                        break;
                    }
                    break;
                }
                if (inslsq.getOpcode() == EnumOpcode.LOAD && islsQValid) {
                    boolean isMatchStoreFound = false;
                    int storeInex = -1;
                    for (int j = i - 1; j >= 0; j--) {
                        InstructionBase insStore = lsq[j];
                        if (insStore.getOpcode() == EnumOpcode.STORE) {
                            if (!insStore.getSrc1().hasValue()) {
                                break;
                            } else if (insStore.getSrc1().getValue() == inslsq.getSrc1().getValue()) {
                                isMatchStoreFound = true;
                                storeInex = j;
                                break;
                            }
                        }
                        matchStoreNotFound = true;
                        loadIndex = i;
                    }

                    if (isMatchStoreFound) {
                        InstructionBase insStore = lsq[storeInex];
                        if (!insStore.getOper0().hasValue()) {
                            continue;
                        } else {
                            loadIndex = i;
                            break;
                        }
                    }
                    if (matchStoreNotFound) {
                        break;
                    }
                }
            }
        }
        if (loadIndex
                > -1) {
            output.setResultValue(store_oper0.getValue(), store_oper0.isFloat());
            output.setProperty("BYPASS LOAD", 1);
            output.setInstruction(lsq[loadIndex]);
            // Send the latch data to the next stage
            output.write();
            core.incIssued();
            lsq[loadIndex] = null;
            for (int shiftIndex = loadIndex; shiftIndex < lsq.length - 1; shiftIndex++) {
                lsq[shiftIndex] = lsq[shiftIndex + 1];
            }
            lsq[lsq.length - 1] = null;
            wrote_output = true;
        }
        if (matchStoreNotFound) {
            output.setResultValue(store_oper0.getValue(), store_oper0.isFloat());
            output.setProperty("ACCESS LOAD", 1);
            output.setInstruction(lsq[loadIndex]);
            // Send the latch data to the next stage
            output.write();
            lsq[loadIndex] = null;
            for (int shiftIndex = loadIndex; shiftIndex < lsq.length - 1; shiftIndex++) {
                lsq[shiftIndex] = lsq[shiftIndex + 1];
            }
            lsq[lsq.length - 1] = null;
            core.incIssued();
            wrote_output = true;
        }
        if (wrote_output || !outputCanAcceptWork(0)) {

            setActivityLocal(diagLsq);
            return;
        }

        // If we find no LOADs to process, see if there are any STORES to ISSUE.
        // An issuable store has known address and data.  To the DCache,
        // an ISSUE STORE passes through to Writeback without modifying memory.  (It will
        // modify memory later on retirement.)  Also, stores that are issued
        // are NOT REMOVED from the LSQ.  Simply mark them as completed.
        // If we issued a STORE, set activity string, bail out
        // Finally, see if there is a STORE that can be COMMITTED (retired).
        // Only the FIRST entry of the LSQ can be retired (to maintain 
        // program order).
        // To the DCache, a COMMIT STORE writes its data value to memory
        // but is NOT passed on to Writeback.
        // Set activity string, return        
        // NOTE:
        // ***
        // Whenever you issue any instruction, be sure to call core.incIssued();
        // This is also the case for the IssueQueue.
        // ***
        for (int i = 0; i < lsq.length; i++) {
            InstructionBase inslsq = lsq[i];
            if (inslsq != null) {
                if (inslsq.getOpcode() == EnumOpcode.LOAD) {
                    continue;
                }

                EnumOpcode opcode = inslsq.getOpcode();
                boolean oper0src = opcode.oper0IsSource();

                Operand oper0 = inslsq.getOper0();
                Operand src1 = inslsq.getSrc1();
                Operand src2 = inslsq.getSrc2();
                // Put operands into array because we will loop over them,
                // searching the pipeline for forwarding opportunities.
                Operand[] operArray = {oper0, src1, src2};

                // For operands that are not registers, getRegisterNumber() will
                // return -1.  We will use that to determine whether or not to
                // look for a given register in the pipeline.
                int[] srcRegs = new int[3];
                // Only want to forward to oper0 if it's a source.
                srcRegs[0] = oper0src ? oper0.getRegisterNumber() : -1;
                srcRegs[1] = src1.getRegisterNumber();
                srcRegs[2] = src2.getRegisterNumber();

                boolean islsQValid = true;
                for (int sn = 0; sn < 3; sn++) {
                    int srcRegNum = srcRegs[sn];
                    // Skip any operands that are not register sources
                    if (srcRegNum < 0) {
                        continue;
                    }
                    // Skip any that already have values
                    if (operArray[sn].hasValue()) {
                        continue;
                    }
                    islsQValid = false;
                    break;
                }
                if (!islsQValid) {
                    continue;
                }
                if (inslsq.hasProperty("COMPLETED") || inslsq.hasProperty("RETIRED")) {
                    continue;
                }
                output.setInstruction(inslsq);
                output.setProperty("ISSUE STORE", 1);
                inslsq.setProperty("COMPLETED", 1);
                output.write();
                core.incIssued();
                wrote_output = true;
            }
        }
        if (wrote_output || !outputCanAcceptWork(0)) {
            setActivityLocal(diagLsq);
            return;
        }
        if (lsq[0] != null && lsq[0].hasProperty("RETIRED")) {
            output.setInstruction(lsq[0]);
            output.setProperty("COMMIT STORE", 1);
            output.write();
            //core.incIssued();
            wrote_output = true;
            lsq[0] = null;
            for (int shiftIndex = 0; shiftIndex < lsq.length - 1; shiftIndex++) {
                lsq[shiftIndex] = lsq[shiftIndex + 1];
            }
            lsq[lsq.length - 1] = null;
        }
        if (wrote_output || !outputCanAcceptWork(0)) {
            setActivityLocal(diagLsq);
            return;
        }
        setActivityLocal(diagLsq);
    }

    private void setActivityLocal(HashMap<Integer, String> diagLsq) {
        Set set = diagLsq.entrySet();
        Iterator iterator = set.iterator();
        List<String> doing = new ArrayList<String>();
        while (iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry) iterator.next();
            doing.add(mentry.getValue().toString());
        }
        setActivity(String.join("\n", doing));
    }
}
