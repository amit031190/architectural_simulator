/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import utilitytypes.EnumOpcode;
import static utilitytypes.EnumOpcode.*;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IPipeReg;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.*;
import utilitytypes.Logger;
import utilitytypes.Operand;

/**
 * Some other students had the idea to store the Latch object containing a
 * dispatched instruction into the IQ. This allowed them to use the pre-
 * existing doForwardingSearch() method to scan for completing inputs for
 * instructions. I consider that to be an excellent alternative approach to what
 * I did here.
 *
 * @author millerti
 */
public class IssueQueue extends PipelineStageBase {

    public IssueQueue(IModule parent) {
        super(parent, "IssueQueue");
    }

    static final EnumSet<EnumOpcode> notExecuteSet
            = EnumSet.of(MUL, DIV, MOD,
                    FADD, FSUB, FMUL, FDIV, FCMP, LOAD, STORE, BRA, CALL, JMP);
    // Data structures...

    @Override
    public void compute() {

        // Check run state
        // Put non-null input into free IQ entry
        // Don't forget to consume() input.
        // Check for forwarding opportunities.
        // Capture desired register inputs available now.
        // Take note of those available next cycle.
        // Select an instruction with valid inputs (or ones that will be
        // forwarded next cycle) to be issued to each output port.
        // Issue instructions
        // Don't forget to write() outputs.
        // Set activity string for diagnostic purposes.  Lines are delimited
        // by newline ('\n').
        ICpuCore core = getCore();
        IGlobals globals = (GlobalData) getCore().getGlobals();
        Latch input = this.readInput(0).duplicate();
        IssueQueueTbl iQTbl = globals.getIssueQueueTbl();

        if (globals.getPropertyInteger(CPU_RUN_STATE) == RUN_STATE_FAULT) {
            input.consume();
            input.setInvalid();
        } else if (globals.getPropertyInteger(CPU_RUN_STATE) == RUN_STATE_FLUSH) {
            iQTbl.flushIQ();
            input.consume();
            input.setInvalid();
        }
        InstructionBase ins = input.getInstruction();

        boolean isNewInsAdded = false;
        if (!ins.isNull()) {
            if (!iQTbl.isFreeSlotAvailable()) {
                addStatusWord("No free slot available");
            } else {
                ins.copyAllPropertiesFrom(input);
                iQTbl.addInstruction(ins);
                isNewInsAdded = true;
                input.consume();
                core.incDispatched();
            }
        }
        boolean[] iQSts = new boolean[iQTbl.getSize()];

        Set<String> fwdSources = core.getForwardingSources();
        HashMap<Integer, IPipeReg> availableRegs = new HashMap<Integer, IPipeReg>();

        for (String fsrc : fwdSources) {
            IPipeReg pipereg = core.getPipeReg(fsrc);
            if (pipereg == null) {
                throw new RuntimeException("No such forwarding source register " + pipereg);
            }
            Latch slave = pipereg.read();
            if (!slave.isNull() && slave.hasResultValue()) {
                int regnum = slave.getResultRegNum();
                availableRegs.put(regnum, pipereg);
            }

            Latch next = pipereg.readNextCycle();
            if (!next.isNull() && next.hasResultValue()) {
                int regnum = next.getResultRegNum();
                availableRegs.put(regnum, pipereg);
            }
        }

        for (int i = 0; i < iQTbl.getSize(); i++) {
            InstructionBase insQ = iQTbl.getInstruction(i);
            insQ.deleteProperty("forward0");
            insQ.deleteProperty("forward1");
            insQ.deleteProperty("forward2");
            EnumOpcode opcode = insQ.getOpcode();
            boolean oper0src = opcode.oper0IsSource();

            Operand oper0 = insQ.getOper0();
            Operand src1 = insQ.getSrc1();
            Operand src2 = insQ.getSrc2();
            Operand[] operArray = {oper0, src1, src2};
            int[] srcRegs = new int[3];
            // Only want to forward to oper0 if it's a source.
            srcRegs[0] = oper0src ? oper0.getRegisterNumber() : -1;
            srcRegs[1] = src1.getRegisterNumber();
            srcRegs[2] = src2.getRegisterNumber();

            for (int sn = 0; sn < 3; sn++) {
                int srcRegNum = srcRegs[sn];
                // Skip any operands that are not register sources
                if (srcRegNum < 0) {
                    continue;
                }
                Set set = availableRegs.entrySet();
                Iterator iterator = set.iterator();
                while (iterator.hasNext()) {
                    Map.Entry mentry = (Map.Entry) iterator.next();
                    IPipeReg pipereg = (IPipeReg) mentry.getValue();

                    Latch slave = pipereg.read();
                    Latch next = pipereg.readNextCycle();
                    if ((int) mentry.getKey() == srcRegNum) {

                        if (slave.getResultRegNum() == srcRegNum) {
                            int value = slave.getResultValue();
                            boolean isfloat = slave.isResultFloat();
                            operArray[sn].setValue(value, isfloat);
                        } else if (next.getResultRegNum() == srcRegNum) {
                            String propname = "forward" + sn;
                            insQ.setProperty(propname, next.getName());
                        }
                    }
                }
            }
        }
        List<IPipeReg> output_regs = getOutputRegisters();
        for (int output_num = 0; output_num < output_regs.size(); output_num++) {
            if (outputCanAcceptWork(output_num)) {
                switch (output_regs.get(output_num).getName()) {
                    case "IQToExecute": {
                        for (int i = 0; i < iQTbl.getSize(); i++) {
                            InstructionBase insQ = iQTbl.getInstruction(i);
                            if (!notExecuteSet.contains(insQ.getOpcode())) {
                                if (consumeIQ(insQ, output_num, i, isNewInsAdded, iQSts)) {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case "IQToBranchResUnit": {
                        for (int i = 0; i < iQTbl.getSize(); i++) {
                            InstructionBase insQ = iQTbl.getInstruction(i);
                            if (insQ.getOpcode().isBranch()) {
                                if (consumeIQ(insQ, output_num, i, isNewInsAdded, iQSts)) {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case "IQToIntMul": {
                        for (int i = 0; i < iQTbl.getSize(); i++) {
                            InstructionBase insQ = iQTbl.getInstruction(i);
                            if (insQ.getOpcode() == EnumOpcode.MUL) {
                                if (consumeIQ(insQ, output_num, i, isNewInsAdded, iQSts)) {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case "IQToIntDiv": {
                        for (int i = 0; i < iQTbl.getSize(); i++) {
                            InstructionBase insQ = iQTbl.getInstruction(i);
                            if (insQ.getOpcode() == EnumOpcode.DIV || insQ.getOpcode() == EnumOpcode.MOD) {
                                if (consumeIQ(insQ, output_num, i, isNewInsAdded, iQSts)) {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case "IQToFloatAddSub": {
                        for (int i = 0; i < iQTbl.getSize(); i++) {
                            InstructionBase insQ = iQTbl.getInstruction(i);
                            if (insQ.getOpcode() == EnumOpcode.FADD || insQ.getOpcode() == EnumOpcode.FSUB || insQ.getOpcode() == EnumOpcode.FCMP) {
                                if (consumeIQ(insQ, output_num, i, isNewInsAdded, iQSts)) {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case "IQToFloatMul": {
                        for (int i = 0; i < iQTbl.getSize(); i++) {
                            InstructionBase insQ = iQTbl.getInstruction(i);
                            if (insQ.getOpcode() == EnumOpcode.FMUL) {
                                if (consumeIQ(insQ, output_num, i, isNewInsAdded, iQSts)) {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    case "IQToFloatDiv": {
                        for (int i = 0; i < iQTbl.getSize(); i++) {
                            InstructionBase insQ = iQTbl.getInstruction(i);
                            if (insQ.getOpcode() == EnumOpcode.FDIV) {

                                if (consumeIQ(insQ, output_num, i, isNewInsAdded, iQSts)) {
                                    break;
                                }
                            }
                        }
                        break;
                    }

                }
            }
        }
        String act = "";
        for (int i = 0; i < iQTbl.getSize(); i++) {
            InstructionBase insQ = iQTbl.getInstruction(i);
            if (iQSts[i]) {
                if (i == iQTbl.getSize() - 1 && isNewInsAdded) {
                    act += insQ.toString() + " [new] [selected]\n";
                } else {
                    act += insQ.toString() + " [selected]\n";
                }
                // iQTbl.freeSlot(i);

            } else {
                if (i == iQTbl.getSize() - 1 && isNewInsAdded) {
                    act += insQ.toString() + " [new]\n";
                } else {
                    act += insQ.toString() + "\n";
                }
            }

        }
        setActivity(act);
        int rmidx = 0;
        for (int i = 0; i < iQSts.length; i++) {
            if (iQSts[i]) {
                iQTbl.freeSlot(rmidx);
                rmidx--;
            }
            rmidx++;
        }
    }

    /**
     *
     * @param insQ
     * @param output_num
     * @param i
     */
    public boolean consumeIQ(InstructionBase insQ, int output_num, int i, boolean isNewInsAdded, boolean[] iQSts) {

        IGlobals globals = (GlobalData) getCore().getGlobals();
        IssueQueueTbl iQTbl = globals.getIssueQueueTbl();
        EnumOpcode opcode = insQ.getOpcode();
        Operand oper0 = insQ.getOper0();
        Operand src1 = insQ.getSrc1();
        Operand src2 = insQ.getSrc2();

        int[] srcRegs = new int[3];
        // Only want to forward to oper0 if it's a source.
        srcRegs[0] = opcode.oper0IsSource() ? oper0.getRegisterNumber() : -1;
        srcRegs[1] = src1.getRegisterNumber();
        srcRegs[2] = src2.getRegisterNumber();
        Operand[] operArray = {oper0, src1, src2};

        // Loop over source operands, looking to see if any can be
        // forwarded to the next stage.
        boolean isIQValid = true;
        Latch output = this.newOutput(output_num);
        for (int sn = 0; sn < 3; sn++) {
            int srcRegNum = srcRegs[sn];
            // Skip any operands that are not register sources
            if (srcRegNum < 0) {
                continue;
            }
            // Skip any that already have values
            if (operArray[sn].hasValue()) {
                continue;
            }

            String propname = "forward" + sn;
            if (!insQ.hasProperty(propname)) {
                // If any source operand is not available
                // now or on the next cycle, then stall.
                //Logger.out.println("Stall because no " + propname);
                //this.setResourceWait(operArray[sn].getRegisterName());
                // Nothing else to do.  Bail out.
                isIQValid = false;
                break;
            } else {

            }
        }
        if (isIQValid) {
            //  Latch output = this.newOutput(output_num);
            // Copy the forward# properties
            output.copyAllPropertiesFrom(insQ);
            // Copy the instruction
            output.setInstruction(insQ);
            // Send the latch data to the next stage
            output.write();
            iQSts[i] = true;
            ICpuCore core = getCore();
            core.incIssued();
            return true;
        }
        return false;
    }
}
